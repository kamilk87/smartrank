var featuresNumber = 0;
$(function () {
    $('.button-add-feature').on('click', function (event) {
        event.preventDefault();
        $('.features-items-container').append(renderFeaturesControls());
        featuresNumber++;
    })
    $('.categories-add').on('click', '.button-delete-feature', function (event) {
        event.preventDefault();
        $(this).parents('.feature-item-container').remove();
        featuresNumber--;
        if (featuresNumber < 0) {
            featuresNumber = 0;
        }
    });

    $('.categories-edit').on('click', '.button-delete-feature', function (event) {
        event.preventDefault();
        var _$self = $(this);
        $.post(deleteUrl + '/' + $(this).data('item-id') + '.json', {id: $(this).data('item-id')}, function (data) {
            console.log(data);
            if (data.result === true) {
                _$self.parents('.feature-item-container').remove();
                featuresNumber--;
                if (featuresNumber < 0) {
                    featuresNumber = 0;
                }
            } else {
                alert('Nie udało sie skasować danego rekordu w bazie danych');
            }
        }).fail(function () {
                _$self.parents('.feature-item-container').remove();
                featuresNumber--;
                if (featuresNumber < 0) {
                    featuresNumber = 0;
                }
        });
    });
});

function renderFeaturesControls() {
    var source = $("#featureAddControlsTemplate").html();
    var template = Handlebars.compile(source);

    var context = {"featureCounter": featuresNumber};
    var html = template(context);
    return html;
}