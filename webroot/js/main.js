var ptc = Storages.initNamespaceStorage('product_to_compare');
var storage = ptc.sessionStorage;
if (!storage.get('items')) {
    storage.set('items', []);
}
$(document).ready(function () {
    var itemsCounter = $('.items-to-compare-number');
    var itemsNumber = storage.get('items');
    itemsCounter.text(itemsNumber.length)
    displayItemsToCompare();

    $('.compare-product').on('click', function () {
        var productId = $(this).data('item-id');
        var name = $(this).data('item-name');
        var image = $(this).data('item-image');
        var item = {
            'id': productId,
            'name': name,
            'image': image
        }
        addItemToCompare(itemsCounter, item);
        displayItemsToCompare();
    });

    $(document).on('click', '.remove-from-compare', function () {
        var idx = $(this).data('item-index');
        removeItemFromCompare(itemsCounter, idx);
        displayItemsToCompare();
    });
    $('.go-to-compare').on('click', function (event) {
        event.preventDefault();
        var currLink = $(this).attr("href") + '/';
        var items = storage.get('items')
        items.forEach(function (element, index, array) {
            currLink += element.id + '/';
        });
        currLink = currLink.substring(0, currLink.length - 1);
        window.location.href = currLink;
    });
});

function addItemToCompare(itemsCounter, item) {
    var items = storage.get('items');
    if (items.length < 3) {
        items.push(item);
        storage.set('items', items);
        itemsCounter.text((itemsCounter.text() * 1) + 1);
    }
}

function removeItemFromCompare(itemsCounter, idx) {
    var items = storage.get('items');
    items.splice(idx, 1);
    storage.set('items', items);
    itemsCounter.text((itemsCounter.text() * 1) - 1);
}

function displayItemsToCompare() {
    $('#cItemsContainer').html('');
    var source = $("#itemsToCompareTemplate").html();
    var template = Handlebars.compile(source);
    var context = {'items': storage.get('items')};
    var html = template(context);

    $('#cItemsContainer').html(html);
}
