<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Collection\Collection;
Use \Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;

/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 *
 * @method \App\Model\Entity\Item[] paginate($object = null, array $settings = [])
 */
class ItemsController extends AppController {

    public function beforeFilter(Event $event) {
        $this->Auth->allow([
            'rank',
            'view',
            'compare'
        ]);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $this->paginate = [
            'contain' => [
                'Categories'
            ]
        ];
        $items = $this->paginate($this->Items);

        $this->set(compact('items'));
        $this->set('_serialize', [
            'items'
        ]);
    }

    /**
     * View method
     *
     * @param string|null $id
     *            Item id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        try {
            $item = $this->Items->get($id, [
                'contain' => [
                    'Categories',
                    'Attributes' => [
                        'Features'
                    ]
                ]
            ]);
        } catch (\Cake\Datasource\Exception\RecordNotFoundException $exception) {
            throw new NotFoundException();
        }
        $this->set('item', $item);
        $this->set('_serialize', [
            'item'
        ]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $item = $this->Items->newEntity();
        $defaultItemCategory = $this->Items->Categories->get(1, [
            'contain' => [
                'Features'
            ]
        ]);
        if ($this->request->is('post')) {
            $item = $this->Items->patchEntity($item, $this->request->getData(), [
                'associated' => [
                    'Attributes',
                    'Attributes.Features',
                    'Categories'
                ]
            ]);
            if ($this->Items->save($item)) {
                $items = $this->Items->find('all')
                        ->contain(['Attributes']);
                foreach ($items as $oneItem) {
                    $oneItem->points = $oneItem->rank;
                    $this->Items->save($oneItem);
                }
                $this->Flash->success(__('Dane zostały zapisane'));

                return $this->redirect([
                            'action' => 'index'
                ]);
            }
            $this->Flash->error(__('Dane nie zostały zapisane'));
        }
        $categories = $this->Items->Categories->find('list', [
            'limit' => 200
        ]);
        $this->set(compact('item', 'categories', 'defaultItemCategory'));
        $this->set('_serialize', [
            'item'
        ]);
    }

    /**
     * Edit method
     *
     * @param string|null $id
     *            Item id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $item = $this->Items->get($id, [
            'contain' => [
                'Categories',
                'Categories.features',
                'Attributes',
                'Attributes.Features'
            ]
        ]);
        $defaultItemCategory = $this->Items->Categories->get($item->category_id, [
            'contain' => [
                'Features'
            ]
        ]);
        if ($this->request->is([
                    'patch',
                    'post',
                    'put'
                ])) {
            $item = $this->Items->patchEntity($item, $this->request->getData(), [
                'associated' => [
                    'Attributes'
                ]
            ]);
            if ($this->Items->save($item)) {

                $items = $this->Items->find('all')
                        ->contain(['Attributes']);
                foreach ($items as $oneItem) {
                    $oneItem->points = $oneItem->rank;
                    $this->Items->save($oneItem);
                }
                $this->Flash->success(__('Dane zostały zapisane'));
                return $this->redirect([
                            'action' => 'index'
                ]);
            }
            $this->Flash->error(__('Dane nie zostały zapisane'));
        }
        $categories = $this->Items->Categories->find('list', [
            'limit' => 200
        ]);
        $this->set(compact('item', 'categories', 'defaultItemCategory', 'itemForm'));
        $this->set('_serialize', [
            'item'
        ]);
    }

    /**
     * Delete method
     *
     * @param string|null $id
     *            Item id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod([
            'post',
            'delete'
        ]);
        $item = $this->Items->get($id);
        if ($this->Items->delete($item)) {
            $this->Flash->success(__('Dane zostały skasowane'));
        } else {
            $this->Flash->error(__('Dane nie zostały skasowane'));
        }

        return $this->redirect([
                    'action' => 'index'
        ]);
    }

    public function rank($category) {
        if (!$category) {
            throw new NotFoundException();
        }
        $this->loadModel('Features');
        $categoryFeatures = $this->Features->find('list', [
            'Category.name' => $category
        ]);

        $criteria = [
            'Categories.name' => $category
        ];
        $queryParamteres = $this->request->getQueryParams();
        $items = $this->Items->find('all', [
                    'contain' => [
                        'Categories',
                        'Attributes' => [
                            'Features'
                        ]
                    ]
                ])->where($criteria);
        if (isset($queryParamteres['best_prices']) and ! empty($queryParamteres['best_prices'])) {
            $items->andWhere(function ($exp, $q) use ($queryParamteres) {
                return $q->newExpr()
                                ->lt('Items.price', substr($queryParamteres['best_prices'], 1));
            });
        }
        $items->order([
            'points' => 'DESC'
        ])->all();
        if (isset($queryParamteres['best_cat']) and ! empty($queryParamteres['best_cat'])) {
            foreach ($items as $oneItem) {
                if (count($oneItem->attributes)) {
                    foreach ($oneItem->attributes as $key => $oneAttrib) {
                        if (in_array($oneAttrib->feature_id, $queryParamteres['best_cat'])) {
                            continue;
                        } else {
                            unset($oneItem->attributes[$key]);
                        }
                    }
                }
            }
        }
        $itemsCollection = new Collection($items->all());
        $itemsCollection = $itemsCollection->sortBy('rank');
        $this->set('isAttrRanking',true);
        $this->set('queryParameters', $queryParamteres);
        $this->set('items', $itemsCollection);
        $this->set('categoryFeatures', $categoryFeatures);
    }

    public function compare() {
        $ids = $this->request->pass;
        $items = $this->Items->find('all')
                ->contain([
                    'Categories',
                    'Categories.Features',
                    'Attributes' => [
                        'Features'
                    ]
                ])
                ->where(function ($exp, $q) use ($ids) {
                    return $exp->in('Items.id', $ids);
                })
                ->order([
                    'points' => 'DESC'
                ])
                ->all();
        if (!$items) {
            throw new NotFoundException();
        }
        $compareCategory = $items->first()->category;
        $this->set('items', $items);
        $this->set('compareCategory', $compareCategory);
    }

}
