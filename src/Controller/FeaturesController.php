<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Features Controller
 *
 * @property \App\Model\Table\FeaturesTable $Features
 *
 * @method \App\Model\Entity\Feature[] paginate($object = null, array $settings = [])
 */
class FeaturesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Categories']
        ];
        $features = $this->paginate($this->Features);

        $this->set(compact('features'));
        $this->set('_serialize', ['features']);
    }

    /**
     * View method
     *
     * @param string|null $id Feature id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $feature = $this->Features->get($id, [
            'contain' => ['Categories', 'Attributes']
        ]);

        $this->set('feature', $feature);
        $this->set('_serialize', ['feature']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $feature = $this->Features->newEntity();
        if ($this->request->is('post')) {
            $feature = $this->Features->patchEntity($feature, $this->request->getData());
            if ($this->Features->save($feature)) {
                $this->Flash->success(__('Dane zostały zapisane'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Dane nie zostały zapisane'));
        }
        $categories = $this->Features->Categories->find('list', ['limit' => 200]);
        $this->set(compact('feature', 'categories'));
        $this->set('_serialize', ['feature']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Feature id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $feature = $this->Features->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $feature = $this->Features->patchEntity($feature, $this->request->getData());
            if ($this->Features->save($feature)) {
                $this->Flash->success(__('Dane zostały zapisane'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Dane nie zostały zapisane'));
        }
        $categories = $this->Features->Categories->find('list', ['limit' => 200]);
        $this->set(compact('feature', 'categories'));
        $this->set('_serialize', ['feature']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Feature id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $feature = $this->Features->get($id);
        if(!$feature){
            $this ->set('result',true);
            $this->render();
        }
        if ($this->Features->delete($feature)) {
            $this->Flash->success(__('Dane zostały skasowane'));
        } else {
            if($this->request->is('ajax')){
                $this ->set('result',false);
            }
            $this->Flash->error(__('Dane nie zostały skasowane'));
        }
        $this->set('_serialize',['result']);
        if(!$this->request->is('ajax')){
            return $this->redirect(['action' => 'index']);
        }
    }
}
