<?php

/**
 * @var \App\View\AppView $this
 */
?>

<div class="container items-compare">
	<div class="page_title">
		<h2>Porównaj produkty</h2>
	</div>
    <div class="">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 comparing_content">
            <div class="row">
                <?php $counter = 1; ?>
				
                <?php foreach ($items as $oneItem): ?>
                    <?= $this->element('Item/linkWithThumb', ['item' => $oneItem, 'place' => $counter]); ?>
                <?php $counter++;?>
                <?php endforeach; ?>
            </div>
            <div class="row">
					<?php foreach ($compareCategory->features as $oneFeature):?>
                <section class="col-md-12">
                    <h3 class="h3_l">
            			 <i class="<?= $oneFeature->icon; ?>"></i> <?= __($oneFeature->name);?>
                    </h3>

                    <table class="comparing_table product_details">
            				<?php $nn=0; 
									foreach ($items as $item):?>
                            		<?php foreach ($item->attributes as $oneAttribute): ?>
                            		<?php if ($oneAttribute->feature_id == $oneFeature->id):?>
                        <tr class="row">
							<?php $nn++; ?>
                            <td class="table_category col-md-3"><?= $item->name; ?></td>
                            <td class="table_bar col-md-8">
                                <div class="progress single_product_bar">
                                    <div class="progress-bar progress-bar-info single_product_color_bar  <?php if($nn == 1) {echo "progres_1";}?> <?php if($nn == 2) {echo "progres_2";}?> <?php if($nn == 3) {echo "progres_3";}?>"
                                         role="progressbar" aria-valuenow="<?= $oneAttribute->rank;?>"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width:<?= $oneAttribute->rank; ?>%">
                                        <a data-toggle="collapse" data-parent="#bs-collapse" class="panel_bar_text collapsed panel_bar_text" href="#feature_<?= $oneAttribute->feature->id . '_' . $item->id ?>">
                                            <?= $this->Text->truncate($oneAttribute->description, 50, ['html' => true,]); ?>
                                        </a>
                                    </div>
                                </div>
                                <div
                                    id="feature_<?= $oneAttribute->feature->id . '_' . $item->id ?>"
                                    class="panel-collapse collapse">
                                    <div class="panel-body blue_background">
                                                <?= $oneAttribute->description; ?>
                                    </div>
                                </div>
                            </td>
                            <td class="col-md-1">
                                <b><?= number_format($oneAttribute->rank,1); ?></b>
                            </td>
                        </tr>
						<?php endif;?>
                            <?php endforeach; ?>
                        <?php endforeach;?>
                    </table>
                </section>
					<?php endforeach;?>
            </div>
        </div>
    </div>
</div>
