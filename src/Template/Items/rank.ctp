<?php

/**
 * @var \App\View\AppView $this
 */
?>
<?php $this->start('headScripts'); ?>
<?= $this->Html->script('items/rank.js'); ?>
<?php $this->end(); ?>
<div class="container">
    <div class="page_title">
        <h2>Ranking</h2>
    </div>
    <div class="row">
        <aside id="sidebar-wrapper" class="col-md-3 up">
            <div class="card">
                <form method="get">
                    <div class="form_section">
                        <h4 class="form_section_title">Najlepsi w kategorii cenowej</h4>
                        <label><input type="radio" value="" name="best_prices"> Wszystkie</label>
                        <label><input type="radio" value="< 2000" name="best_prices"  
                            <?php if(isset($queryParameters['best_prices']) and $queryParameters['best_prices'] =="< 2000"): ?> checked="checked"<?php endif;?>
                                      > Do 2000zł</label>
                        <label><input type="radio" value="< 1500" name="best_prices"
                            <?php if(isset($queryParameters['best_prices']) and $queryParameters['best_prices'] =="< 1500"): ?> checked="checked"<?php endif;?>
                                      > Do 1500zł</label>
                        <label><input type="radio" value="< 1000" name="best_prices"
                            <?php if(isset($queryParameters['best_prices']) and $queryParameters['best_prices'] =="< 1000"): ?> checked="checked"<?php endif;?>
                                      > Do 1000zł</label>
                        <label><input type="radio" value="< 500" name="best_prices"
                            <?php if(isset($queryParameters['best_prices']) and $queryParameters['best_prices'] =="< 500"): ?> checked="checked"<?php endif;?> 
                                      > Do 500zł</label>
                    </div>
                    <div class="form_section">
                        <h4 class="form_section_title">Wyświetl parametry:</h4>
                        <?php foreach ($categoryFeatures as $key => $value):?>
                        <label>
                            <input type="checkbox" value="<?= $key; ?>" name="best_cat[]"
                                <?php if(isset($queryParameters['best_cat']) and in_array($key, $queryParameters['best_cat'])):?> checked="checked" <?php endif;?>
                                   > <?= $value;?>
                        </label>
                        <?php endforeach;?>
                    </div>
                    <div class="text-center">
                        <p>
                            <button class="btn btn-primary button-rank-filter" id=""><?= __('Zastosuj');?></button>
                        </p>
                    </div>
            </div>
            </form>
        </aside>

        <div id="main-wrapper" class="col-md-9">

            <div class="row">
                <div class="col-md-12 up">
                    <?php if(isset($items)):?>
                        <?php $counter = 1;?>
                        <?php foreach ($items as $oneItem):?>
                            <?= $this->element('Item/oneItem',['item' => $oneItem,'place' => $counter]);?>
                    <p></p>
                            <?php $counter++; ?>
                            <?php endforeach;?>
                        <?php else:?>
                    <div class="lead">
                            <?= __('Brak przedmiotów z wybranej kategorii');?>
                    </div>
                    <?php endif;?>
                </div>
            </div>

        </div>
    </div>
</div>
