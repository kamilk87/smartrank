<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>
<div class="container">
	<div class="page_title">
		<h2>Szczegóły produktu</h2>
	</div>
	<div class="row">
		<div class="col-md-12 up">
            <?= $this->element('Item/oneItem');?>
        </div>
	</div>

	<div class="row">
		<div class="col-md-12 up">
			<div class="card">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    				<?= $item->description;?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>