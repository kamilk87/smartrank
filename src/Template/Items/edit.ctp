<?php
/**
 * @var \App\View\AppView $this
 */
?>
<?php $this->start('headScripts'); ?>
<?= $this->Html->script('/ckeditor/ckeditor.js'); ?>
<?php $this->end(); ?>
<div class="container items-add">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2 class="text-left"><?= __('Edycja przedmiotów') ?></h2>
            <?=$this->Form->create($item, ['type' => 'file','class' => 'form', 'associated' =>[ 'Attributes','Attributes.Features']])?>
            <div class="form-group">
                <?=$this->Form->control('image', ['type' => 'file','class' => 'form-control','label' => ['text' => 'Zdjęcie produktu','class' => 'label'],'required' => false]);?>
            </div>
			<div class="form-group">
                <?=$this->Form->control('name', ['class' => 'form-control','label' => ['text' => __('Nazwa'),'class' => 'label'],'required' => true]);?>
            </div>
			<div class="form-group">
                <?=$this->Form->control('price', ['class' => 'form-control','label' => ['text' => __('Cena'),'class' => 'label'],'required' => true]);?>
            </div>
			<div class="form-group">
                <?=$this->Form->control('description', ['class' => 'form-control','label' => ['text' => __('Opis'),'class' => 'label'],'required' => true]);?>
            </div>
			<div class="form-group">
                <?=$this->Form->control('category_id', ['options' => $categories,'default' => 1,'class' => 'form-control','label' => ['text' => __('Kategoria'),'class' => 'label'],'required' => true]);?>
            </div>
			<div class="form-group">
                <?=$this->Form->control('link', ['class' => 'form-control','label' => ['text' => __('Link do produktu'),'class' => 'label'],'required' => true]);?>
            </div>
			<div class="item-attributes-form-controls-container">
                <?php $counter = 0;?>
                <?php foreach ($item->attributes as $oneItem): ?>
                <div class="section_attributes_border">
                    <div class="form-group">
                        <?=$this->Form->control("attributes[$counter].points", ['type' => 'number','class' => 'form-control','label' => ['text' => __('Punkty atrybutu: ') . $oneItem->feature->name,'class' => 'label'],'required' => true,'value'=> $oneItem->points]);?>
                    </div>
				    <div class="form-group">
                        <?=$this->Form->control("attributes[$counter].description", ['type' => 'textarea','class' => 'form-control','label' => ['text' => __('Opis atrybutu: ') . $oneItem->feature->name,'class' => 'label'],'required' => true,'value'=> $oneItem->description]);?>
                    </div>
                       <?=$this->Form->control("attributes[$counter].id", ['type' => 'hidden','value' => $oneItem->id,'required' => true]);?>
                       <?php  $counter++;?>
                </div>
                <?php endforeach; ?>
            </div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-group no_dashed_border">
                        <?= $this->Form->submit(__('Zapisz'), ['class' => 'btn btn-primary']); ?>
                        <?php echo $this->Form->control('comments.0.id') ;?>
                    </div>
				</div>
			</div>
            <?= $this->Form->end(); ?>
        </div>
	</div></div>
<script>
    $(function () {
        CKEDITOR.replace( 'description' );
    });
</script>
