<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category[]|\Cake\Collection\CollectionInterface $categories
 */
?>
<div class="container">
    <div class="">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2 class="text-left"><?= __('Przedmioty') ?></h2>
            <table class="table table-bordered table-responsive table-striped col-md-12">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id',__('Numer')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('image', __('Zdjęcie')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name', __('Nazwa')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('price', __('Cena')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('category_id', __('Katetgoria')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('points', __('Punkty')) ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($items as $item): ?>
                    <tr>
                        <td><?= $this->Number->format($item->id) ?></td>
                        <td><?= $this->Html->image('/Items/image/'.$item->image,['class'=>'img-responsive','width'=> '173px']) ?></td>
                        <td><?= h($item->name) ?></td>
                        <td><?= $this->Number->format($item->price) ?></td>
                        <td><?= $item->has('category') ? $this->Html->link($item->category->name, ['controller' => 'Categories', 'action' => 'view', $item->category->id]) : '' ?></td>
                        <td><?= $this->Number->format($item->points) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Podgląd'), ['action' => 'view', $item->id]) ?>
                            <?= $this->Html->link(__('Edycja'), ['action' => 'edit', $item->id]) ?>
                            <?= $this->Form->postLink(__('Kasowanie'), ['action' => 'delete', $item->id], ['confirm' => __('Czy napewno usunąć # {0}?', $item->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('Pierwsza')) ?>
                    <?= $this->Paginator->prev('< ' . __('Poprzednia')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Następna') . ' >') ?>
                    <?= $this->Paginator->last(__('Ostatnia') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Strona {{page}} z {{pages}}, pokazuje {{current}}  z {{count}} wszystkich')]) ?></p>
            </div>

        </div>
    </div>
</div>
