<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category[]|\Cake\Collection\CollectionInterface $categories
 */
?>
<div class="container">
    <div class="">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2 class="text-left"><?= __('Kategorie') ?></h2>
            <table class="table table-bordered table-responsive table-striped">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id',__('Numer')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name',__('Nazwa')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created',__('Data utworzenia')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('modified',__('Data ostatniej modyfikacji')) ?></th>
                    <th scope="col" class="actions"><?= __('Działania') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($categories as $category): ?>
                    <tr>
                        <td><?= $this->Number->format($category->id) ?></td>
                        <td><?= h($category->name) ?></td>
                        <td><?= h($category->created) ?></td>
                        <td><?= h($category->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Edycja'), ['action' => 'edit', $category->id]) ?>
                            <?= $this->Form->postLink(__('Kasowanie'), ['action' => 'delete', $category->id], ['confirm' => __('Czy jesteś pewny, że chcesz usunąć  # {0}?', $category->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('Pierwsza')) ?>
                    <?= $this->Paginator->prev('< ' . __('Poprzednia')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Następna') . ' >') ?>
                    <?= $this->Paginator->last(__('Ostatnia') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Strona {{page}} z {{pages}}, pokazuje {{current}}  z {{count}} wszystkich')]) ?></p>
            </div>

        </div>
    </div>
</div>
