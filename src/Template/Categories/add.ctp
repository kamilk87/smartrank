<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category[]|\Cake\Collection\CollectionInterface $categories
 */
?>
<?php $this->start('headScripts'); ?>
<?= $this->Html->script('categories/add.js'); ?>
<?php $this->end(); ?>
<div class="container categories-add">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2 class="text-left"><?= __('Dodawanie kategorii') ?></h2>
            <?= $this->Form->create($category, ['class' => 'form']) ?>
            <div class="form-group">
                <?= $this->Form->control('name', [
                    'class' => 'form-control',
                    'label' => [
                        'text' => __('Nazwa'),
                        'class' => 'label'
                    ],
                    'required' => true,
                ]); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('description', [
                    'type' => 'textarea',
                    'class' => 'form-control',
                    'label' => [
                        'text' => __('Opis'),
                        'class' => 'label'
                    ],
                    'required' => true,
                ]); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('icon', [
                    'class' => 'form-control',
                    'label' => [
                        'text' => __('Ikona'),
                        'class' => 'label'
                    ],
                    'required' => true,
                ]); ?>
            </div>
            <div class="form-group features-items-container">
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <?= $this->Form->button(__('Dodaj cechę'), ['class' => 'button-add-feature btn btn-secondary add_category_btn']); ?>
                        <?= $this->Form->submit(__('Zapisz'), ['class' => 'btn btn-primary save_category_btn']); ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<script id="featureAddControlsTemplate" type="text/x-handlebars-template">
    <div class="feature-item-container panel panel-default">
        <div class="form-group">
            <div class="input text required">
                <label for="features-{{featureCounter}}-name" class="label"><?= __('Nazwa'); ?></label>
                <input name="features[{{featureCounter}}][name]" required="required" maxlength="255"
                       id="features-{{featureCounter}}-name" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="input text required">
                <label for="features-{{featureCounter}}-icon" class="label"><?= __('Ikona'); ?></label>
                <input name="features[{{featureCounter}}][icon]" required="required" maxlength="255"
                       id="features-{{featureCounter}}-icon" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="input text required">
                <label for="features-{{featureCounter}}-description" class="label"><?= __('Opis'); ?></label>
                <input name="features[{{featureCounter}}][description]" required="required" maxlength="255"
                       id="features-{{featureCounter}}-description" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <button class="button-delete-feature btn btn-danger"><?= __('Usuń cechę') ?></button>
        </div>
    </div>
</script>
