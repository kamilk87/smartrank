<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category[]|\Cake\Collection\CollectionInterface $categories
 */
?>
<div class="contianer categories-view">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 panel panel-default">
            <nav class="panel-body" id="actions-sidebar">
                <h2 class="text-left"><?= __('Akcje') ?></h2>
                <ul class="list-group">
                    <li class="list-group-item"><?= $this->Html->link(__('Edycja kategorii'), ['action' => 'edit', $category->id]) ?> </li>
                    <li class="list-group-item"><?= $this->Form->postLink(__('Kasowanie kategorii'), ['action' => 'delete', $category->id], ['confirm' => __('Czy napewno usunąć # {0}?', $category->id)]) ?> </li>
                    <li class="list-group-item"><?= $this->Html->link(__('Lista Kategorii'), ['action' => 'index']) ?></li>
                    <li class="list-group-item"><?= $this->Html->link(__('Nowa Katetgoria'), ['action' => 'add']) ?></li>
                    <li class="list-group-item"><?= $this->Html->link(__('Lista Przedmiotów'), ['controller' => 'Items', 'action' => 'index']) ?></li>
                    <li class="list-group-item"><?= $this->Html->link(__('Nowy Przedmiot'), ['controller' => 'Items', 'action' => 'add']) ?></li>
                </ul>
            </nav>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <h3 class="text-left"><?= __('Podgląd kategorii'); ?> <?= h($category->name) ?></h3>
            <h4><?= __('Opis') ?></h4>
            <?= $this->Text->autoParagraph(h($category->description)); ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h4><?= __('Cechy') ?></h4>
                    <?php if (!empty($category->features)): ?>
                        <table class="table table-bordered table-striped table-responsive">
                            <thead>
                            <tr>
                                <th><?= __('Nazwa') ?></th>
                                <th><?= __('Ikona') ?></th>
                                <th><?= __('Akcje') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($category->features as $features): ?>
                                <tr>
                                    <td><?= h($features->name) ?></td>
                                    <td class="text-center">  <i class="fa-2x <?= $features->icon; ?>" aria-hidden="true"></i></td>
                                    <td class="actions">
                                        <?= $this->Html->link(__('Podgląd'), ['controller' => 'Features', 'action' => 'view', $features->id]) ?>
                                        <?= $this->Html->link(__('Edycja'), ['controller' => 'Features', 'action' => 'edit', $features->id]) ?>
                                        <?= $this->Form->postLink(__('Kasowanie'), ['controller' => 'Features', 'action' => 'delete', $features->id], ['confirm' => __('Czy napewno skasować # {0}?', $features->id)]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>