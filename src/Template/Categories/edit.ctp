<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category[]|\Cake\Collection\CollectionInterface $categories
 */
?>
<?php $this->start('headScripts'); ?>
<script>
    var deleteUrl = '<?= $this->Url->build(['controller' => 'Features', 'action'=> 'delete']);?>';
</script>
<?= $this->Html->script('categories/add.js'); ?>
<?php $this->end(); ?>
<div class="container categories-edit">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2 class="text-left"><?= __('Edycja kategorii') ?></h2>
            <?= $this->Form->create($category, ['class' => 'form']) ?>
            <div class="form-group">
                <?= $this->Form->control('name', [
                    'class' => 'form-control',
                    'label' => [
                        'text' => __('Nazwa'),
                        'class' => 'label'
                    ],
                    'required' => true,
                ]); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('description', [
                    'type' => 'textarea',
                    'class' => 'form-control',
                    'label' => [
                        'text' => __('Opis'),
                        'class' => 'label'
                    ],
                    'required' => true,
                ]); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('icon', [
                    'class' => 'form-control',
                    'label' => [
                        'text' => __('Ikona'),
                        'class' => 'label'
                    ],
                    'required' => true,
                ]); ?>
            </div>
            <div class="form-group features-items-container">
                <?php foreach ($category->features as $oneFeature): ?>
                    <div class="feature-item-container panel panel-default section_attributes_border">
                        <div class="form-group">
                            <div class="input text required">
                                <label for="features-<?= $oneFeature->id ?>-name"
                                       class="label"><?= __('Nazwa'); ?></label>
                                <input name="features[<?= $oneFeature->id ?>][name]" required="required" maxlength="255"
                                       id="features-<?= $oneFeature->id ?>-name" type="text" class="form-control"
                                       value="<?= $oneFeature->name ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input text required">
                                <label for="features-<?= $oneFeature->id ?>-icon"
                                       class="label"><?= __('Ikona'); ?></label>
                                <input name="features[<?= $oneFeature->id ?>][icon]" required="required" maxlength="255"
                                       id="features-<?= $oneFeature->id ?>-icon" type="text" class="form-control"
                                       value="<?= $oneFeature->icon ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input text required">
                                <label for="features-<?= $oneFeature->id ?>-description"
                                       class="label"><?= __('Opis'); ?></label>
                                <input name="features[<?= $oneFeature->id ?>][description]" required="required"
                                       maxlength="255"
                                       id="features-<?= $oneFeature->id ?>-description" type="text" class="form-control"
                                       value="<?= $oneFeature->description ?>">
                            </div>
                        </div>
                        <input type="hidden" name="features[<?= $oneFeature->id ?>][id]" value="<?= $oneFeature->id ?>">
                        <div class="form-group">
                            <button class="button-delete-feature btn btn-danger" data-item-id="<?= $oneFeature->id ?>"><?= __('Usuń cechę') ?></button>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <?= $this->Form->button(__('Dodaj cechę'), ['class' => 'button-add-feature btn btn-secondary']); ?>
                        <?= $this->Form->submit(__('Zapisz'), ['class' => 'btn btn-primary save_category_btn']); ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<script id="featureAddControlsTemplate" type="text/x-handlebars-template">
    <div class="feature-item-container panel panel-default">
        <div class="form-group">
            <div class="input text required">
                <label for="features-{{featureCounter}}-name" class="label"><?= __('Nazwa'); ?></label>
                <input name="features[{{featureCounter}}][name]" required="required" maxlength="255"
                       id="features-{{featureCounter}}-name" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="input text required">
                <label for="features-{{featureCounter}}-icon" class="label"><?= __('Ikona'); ?></label>
                <input name="features[{{featureCounter}}][icon]" required="required" maxlength="255"
                       id="features-{{featureCounter}}-icon" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="input text required">
                <label for="features-{{featureCounter}}-description" class="label"><?= __('Opis'); ?></label>
                <input name="features[{{featureCounter}}][description]" required="required" maxlength="255"
                       id="features-{{featureCounter}}-description" type="text" class="form-control">
            </div>
        </div>
        <input type="hidden" name="{{featureCounter}}][id]" value="{{featureCounter}}">
        <div class="form-group">
            <button class="button-delete-feature btn btn-danger" data-item-id="{{featureCounter}}"><?= __('Usuń cechę') ?></button>
        </div>
    </div>
</script>
