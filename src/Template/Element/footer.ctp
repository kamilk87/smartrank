<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; 2017 <a href="http://tlenet.pl">Tlenet.pl</a></p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<?= $this->Html->script('/vendor/popper/popper.min.js'); ?>
<?= $this->Html->script('/vendor/bootstrap/js/bootstrap.min.js'); ?>
<?= $this->Html->script('main.js'); ?>
<?= $this->fetch('bottomScripts'); ?>
</body>

</html>
