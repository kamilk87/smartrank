<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="card product_box">
    <div class="row">
        <div class="col-lg-3 col-md-12 col-sx-12">
            <div style="width: 33%; display: block; margin: 0 auto; text-align: center;">
                <span class="badge product_place "><?= isset($place)? $place :$item->position ?></span>
            </div>
			<p class="product_score"><?= isset($isAttrRanking)? number_format($item->rank,1): number_format($item->points,1);?> - Smart Rank Score</p>
        </div>
		<div class="col-lg-9 col-md-12 col-sx-12">
			<h3 class="product_title">
                <a
                    href="<?php echo $this->Url->build(["controller" => "Items", "action" => "view", $item->id]); ?>">
                    <?= $item->name ?>
                </a>
            </h3>
		</div>
        <div class="col-lg-3 col-md-12 col-sx-12">
            <?= $this->Html->image('/Items/image/'.$item->image,['class'=>'img-responsive','width'=> '173px']) ?>
            <p class="card-text product-price">
                <b><?= $item->price; ?> zł</b>
            </p>
            <a href="<?= $item->link; ?>" class="btn btn-primary btn-block">Przejdź
                do sklepu</a> <a href="#"
                                 class="btn btn-lg btn-secondary btn-block compare-product"
                                 data-item-id="<?= $item->id; ?>" data-item-name="<?= $item->name; ?>" 
                                 data-item-image='<?= $this->Html->image('/Items/image/'.$item->image,['class'=>'img-responsive','height'=> '64px']) ?>'>Dodaj
                do porównania</a>
        </div>
        <div class="col-lg-9 col-md-12 col-sx-12">
            <div class="panel-group wrap" id="bs-collapse">
    <div class="comparing_section row">    
            <?php foreach ($item->attributes as $oneAttribute): ?>
        <p class="col-md-3 features-bar">
            <?= $oneAttribute->feature->name; ?> <i class="<?= $oneAttribute->feature->icon; ?>"></i>
        </p>
        <div class="panel_bars col-lg-8 col-md-7 col-sx-7">
            <div class="panel">
                <div class="panel-heading progress single_product_bar">
                    <div class="panel-title progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?= $oneAttribute->rank;?>" aria-valuemin="0" aria-valuemax="100" style="width:<?= $oneAttribute->rank; ?>%">
                        <a data-toggle="collapse" class="panel_bar_text collapsed" data-parent="#bs-collapse" href="#feature_<?= $oneAttribute->feature->id . '_' . $item->id ?>" aria-expanded="false">
                            <?= $this->Text->truncate($oneAttribute->description, 50, ['html' => true,]); ?>
                        </a>
                    </div>
                </div>
                <div id="feature_<?= $oneAttribute->feature->id . '_' . $item->id ?>" class="panel-collapse collapse">
                    <div class="panel-body blue_background">
                        <?= $oneAttribute->description; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="table_rating col-lg-1 col-md-2 col-sx-2">
            <?= number_format($oneAttribute->rank,1); ?>
        </div>
            <?php endforeach; ?>
    </div>
</div>
        </div>

    </div>
</div>
