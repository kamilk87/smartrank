<div class="col-md-4 compare-thumb">
    <div class="row">
        <div class="col-md-12">
            <div class="rank-points-container">
                <div class="smart-rank-score text-center">
                    <div style="width: 33%; display: block; margin: 0 auto;">
						<span class="badge product_place "><?= isset($place)? $place :$item->position ?></span>
					</div>
					<p class="product_score"><?= number_format($item->points,1);?> - Smart Rank Score</p>
                </div>
            </div>
            <h3 class="product_title">
                <a
                    href="<?php echo $this->Url->build(["controller" => "Items", "action" => "view", $item->id]); ?>">
                    <?= $item->name ?>
                </a>
            </h3>
				<?= $this->Html->image('/Items/image/'.$item->image,['class'=>'img-responsive','width'=> '173px']) ?>
            <p class="card-text product-price">
                <b><?= $item->price; ?> zł</b>
            </p>
            <a href="<?= $item->link; ?>" class="btn btn-primary btn-block shop_button <?php if($place == 1) {echo "progres_1";}?> <?php if($place == 2) {echo "progres_2";}?> <?php if($place == 3) {echo "progres_3";}?>">Przejdź
            do sklepu</a>
        </div>
    </div>
</div>
