<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand"
           href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'display', 'home']); ?>">SMARTRANK</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <?= __('Ranking'); ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <?php if ($categories): ?>
                            <?php foreach ($categories as $oneCategory): ?>
                                <?= $this->Html->link($oneCategory->name, ['controller' => 'items', 'action' => 'rank', $oneCategory->name], ['class' => 'dropdown-item']); ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <?= $this->Html->link(_('Artykuły'), [
                        '#' => 'Articles'
                    ], [
                        'class' => 'nav-link'
                    ]); ?>
                </li>
                <li class="nav-item">
                    <?= $this->Html->link(_('Experci'), [
                        '#' => 'Experts'
                    ], [
                        'class' => 'nav-link'
                    ]); ?>
                </li>
                <li class="nav-item">
                    <?= $this->Html->link(_('Testy'), [
                        '#' => 'Tests'
                    ], [
                        'class' => 'nav-link'
                    ]); ?>
                </li>

                <li class="nav-item">
                    <?= $this->Html->link(_('Sklep'), [
                        '#' => 'Shop'
                    ], [
                        'class' => 'nav-link'
                    ]); ?>
                </li>
                <?php if (isset($user) and $user->role->name == 'admin'): ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <?= __('Przedmioty'); ?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <?= $this->Html->link(_('Dodaj'), [
                            'controller' => 'Items',
                            'action' => 'add'
                        ], [
                            'class' => 'dropdown-item'
                        ]); ?>
                        <?= $this->Html->link(_('Lista'), [
                            'controller' => 'Items',
                            'action' => 'index'
                        ], [
                            'class' => 'dropdown-item'
                        ]); ?>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <?= __('Kategorie'); ?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <?= $this->Html->link(_('Dodaj'), [
                            'controller' => 'Categories',
                            'action' => 'add'
                        ], [
                            'class' => 'dropdown-item'
                        ]); ?>
                        <?= $this->Html->link(_('Lista'), [
                            'controller' => 'Categories',
                            'action' => 'index'
                        ], [
                            'class' => 'dropdown-item'
                        ]); ?>
                        </div>
                    </li>
<!--                    <li class="nav-item">
                        <?= $this->Html->link(_('Cechy'), [
                            'controller' => 'Features',
                            'action' => 'index'
                        ], [
                            'class' => 'nav-link'
                        ]); ?>
                    </li>-->
                    <li class="nav-item">
                        <?= $this->Html->link(_('Wyloguj'), [
                            'controller' => 'Users',
                            'action' => 'logout'
                        ], [
                            'class' => 'nav-link'
                        ]); ?>
                    </li>
                <?php else: ?>
                    <li class="nav-item">
                        <?= $this->Html->link(_('Zaloguj'), [
                            'controller' => 'Users',
                            'action' => 'login'
                        ], [
                            'class' => 'nav-link'
                        ]); ?>
                    </li>
                <?php endif; ?>
                <li class="nav-item dropdown ">
                    <a class="nav-link" href="#"><img width="20"
                                                      src="http://findicons.com/files/icons/860/social_media/32/youtube_white.png"></a>
                </li>

                <li class="nav-item dropdown ">
                    <a class="nav-link zeropading" href="#"><img width="20"
                                                                 src="http://findicons.com/files/icons/860/social_media/32/facebook_white.png"></a>
                </li>
                <li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="comparedItemsContainer" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <?= __('Porównywane'); ?> (<span class="items-to-compare-number">0</span>)
                    </a>
                    <div class="dropdown-menu dropdown-menu-right text-center compare-list"
                         aria-labelledby="comparedItemsContainer" id="">
                        <div id="cItemsContainer"></div>
                        <p>
                            <?= $this->Html->link(_('Porównaj'), [
                                'controller' => 'items',
                                'action' => 'compare'
                            ], [
                                'class' => 'btn btn-secondary go-to-compare'
                            ]); ?>
                        </p>
                    </div>
                </li>
                </li>
            </ul>
        </div>
    </div>
</nav>
<script id="itemsToCompareTemplate" type="text/x-handlebars-template">
    <table class="table table-striped">
        {{#each items}}
        <tr class="compared-product-{{id}}">
            <td>
                <div class="compared_product_img">{{{image}}}</div>
                <div class="compared_product_name">{{name}}</div>
                <div class="compared_product_btn"><button class="remove-from-compare btn btn-danger float-right"
                        data-item-id="{{id}}" data-item-index="{{@index}}"><?= __('Usuń') ?></button></div>
            </td>
        </tr>
        {{/each}}
    </table>
</script>
