<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category[]|\Cake\Collection\CollectionInterface $categories
 */
?>
<div class="contianer features-index">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 panel panel-default">
            <nav class="panel-body" id="actions-sidebar">
                <h3 class="text-left"><?= __('Akcje') ?></h3>
                <ul class="list-group">
                    <li class="list-group-item"><?= $this->Html->link(__('Nowa cecha'), ['action' => 'add']) ?></li>
                    <li class="list-group-item"><?= $this->Html->link(__('Lista kategorii'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
                    <li class="list-group-item"><?= $this->Html->link(__('Nowa kategoria'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
                </ul>
            </nav>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <h3 class="text-left"><?= __('Cechy') ?></h3>
            <table class="table table-bordered table-responsive table-striped">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id',__('Numer')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name', __('Nazwa')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('icon', __('Ikona')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('category_id', __('Kategoria')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created',__('Data utworzenia')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('modified',__('Data ostatniej modyfikacji')) ?></th>
                    <th scope="col" class="actions"><?= __('Działania') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($features as $feature): ?>
                    <tr>
                        <td><?= $this->Number->format($feature->id) ?></td>
                        <td><?= h($feature->name) ?></td>
                        <td><?= h($feature->icon) ?></td>
                        <td><?= $feature->has('category') ? $this->Html->link($feature->category->name, ['controller' => 'Categories', 'action' => 'view', $feature->category->id]) : '' ?></td>
                        <td><?= h($feature->created) ?></td>
                        <td><?= h($feature->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Podgląd'), ['action' => 'view', $feature->id]) ?>
                            <?= $this->Html->link(__('edycja'), ['action' => 'edit', $feature->id]) ?>
                            <?= $this->Form->postLink(__('Kasowanie'), ['action' => 'delete', $feature->id], ['confirm' => __('Czy napewno usunąć # {0}?', $feature->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('Pierwsza')) ?>
                    <?= $this->Paginator->prev('< ' . __('Poprzednia')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Następna') . ' >') ?>
                    <?= $this->Paginator->last(__('Ostatnia') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Strona {{page}} z {{pages}}, pokazuje {{current}}  z {{count}} wszystkich')]) ?></p>
            </div>

        </div>
    </div>
</div>
