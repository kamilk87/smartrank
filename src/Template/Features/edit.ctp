<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="contianer features-view">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 panel panel-default">
            <nav class="panel-body" id="actions-sidebar">
                <h3 class="text-left"><?= __('Akcje') ?></h3>
                <ul class="list-group">
                    <li class="list-group-item"><?= $this->Form->postLink(
                            __('Kasowanie'),
                            ['action' => 'delete', $feature->id],
                            ['confirm' => __('Czy napewno usunąć # {0}?', $feature->id)]
                        )
                        ?></li>
                    <li class="list-group-item"><?= $this->Html->link(__('Podgląd'), ['action' => 'view', $feature->id]) ?> </li>
                    <li class="list-group-item"><?= $this->Html->link(__('Lista cech'), ['action' => 'index']) ?> </li>
                    <li class="list-group-item"><?= $this->Html->link(__('Nowa cecha'), ['action' => 'add']) ?> </li>
                    <li class="list-group-item"><?= $this->Html->link(__('Lista kategorii'), ['controller' => 'Categories', 'action' => 'index']) ?> </li>
                    <li class="list-group-item"><?= $this->Html->link(__('Nowa kategoria'), ['controller' => 'Categories', 'action' => 'add']) ?> </li>
                </ul>
            </nav>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <h3 class="text-left"><?= __('Edycja cechy: '); ?><?= h($feature->name) ?></h3>
            <?= $this->Form->create($feature, ['class' => 'form']); ?>
            <div class="form-group">
                <?= $this->Form->control('name', [
                    'class' => 'form-control',
                    'label' => [
                        'text' => __('Nazwa'),
                        'class' => 'label'
                    ],
                    'required' => true,
                ]); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('icon', [
                    'class' => 'form-control',
                    'label' => [
                        'text' => __('Ikona'),
                        'class' => 'label'
                    ],
                    'required' => true,
                ]); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('description', [
                    'class' => 'form-control',
                    'label' => [
                        'text' => __('Opis'),
                        'class' => 'label'
                    ],
                    'required' => true,
                ]); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('category_id', [
                    'options' => $categories,
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <?= $this->Form->submit(__('Zapisz'), ['class' => 'btn btn-primary']); ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
