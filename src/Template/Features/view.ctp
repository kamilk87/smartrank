<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="contianer features-view">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 panel panel-default">
            <nav class="panel-body" id="actions-sidebar">
                <h3 class="text-left"><?= __('Akcje') ?></h3>
                <ul class="list-group">
                    <li class="list-group-item"><?= $this->Html->link(__('Edycja cechy'), ['action' => 'edit', $feature->id]) ?> </li>
                    <li class="list-group-item"><?= $this->Form->postLink(__('Kasowanie'), ['action' => 'delete', $feature->id], ['confirm' => __('Are you sure you want to delete # {0}?', $feature->id)]) ?> </li>
                    <li class="list-group-item"><?= $this->Html->link(__('Lista cech'), ['action' => 'index']) ?> </li>
                    <li class="list-group-item"><?= $this->Html->link(__('Nowa cecha'), ['action' => 'add']) ?> </li>
                    <li class="list-group-item"><?= $this->Html->link(__('Lista kategorii'), ['controller' => 'Categories', 'action' => 'index']) ?> </li>
                    <li class="list-group-item"><?= $this->Html->link(__('Nowa kategoria'), ['controller' => 'Categories', 'action' => 'add']) ?> </li>
                </ul>
            </nav>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <h3 class="text-left"><?= __('Podgląd cechy: '); ?><?= h($feature->name) ?></h3>
            <h4><?= __('Opis') ?></h4>
            <?= $this->Text->autoParagraph(h($feature->description)); ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table class="table table-bordered table-striped table-responsive">
                        <tr>
                            <th scope="row"><?= __('Nazwa') ?></th>
                            <td><?= h($feature->name) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Ikona') ?></th>
                            <td><?= h($feature->icon) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Kategoria') ?></th>
                            <td><?= $feature->has('category') ? $this->Html->link($feature->category->name, ['controller' => 'Categories', 'action' => 'view', $feature->category->id]) : '' ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
