<?php
/**
 * @var App\View\AppView $this
 */
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <?= $this->Html->charset('utf-8') ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <?= $this->fetch('meta'); ?>
        <title>SmartRank - Nowoczesna porównywarka produktów</title>
        <?= $this->Html->script('/vendor/jquery/jquery.min.js'); ?>
        <script>
            $(document).ajaxSend(function(e, xhr, settings) {
                xhr.setRequestHeader('X-CSRF-Token', '<?= $this->request->getParam('_csrfToken') ?>');
            });
        </script>
        <?= $this->Html->script('js.storage.min.js'); ?>
        <?= $this->Html->script('handlebars-v4.0.10.js'); ?>
        <?= $this->Html->css('/vendor/bootstrap/css/bootstrap.min.css'); ?>
        <?= $this->Html->css('https://fonts.googleapis.com/css?family=Lato:300,400,700'); ?>
        <?= $this->Html->css('https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&amp;subset=latin-ext'); ?>
        <?= $this->Html->css('style.css'); ?>
        <?= $this->Html->css('/vendor/font-awesome-4.7.0/css/font-awesome.css'); ?>
        <?= $this->fetch('styles'); ?>
        <?= $this->fetch('headScripts'); ?>
        <?= $this->fetch('script'); ?>
        <?= $this->fetch('css'); ?>
    </head>

    <body>
<?php
echo $this->element('header');
echo $this->fetch('upperSection');
if ($this->request->getSession()->check('Flash.flash')) {
    echo '<div class="container">';
    echo '<div class="row"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
    echo $this->Flash->render();
    echo '</div> </div>';
    echo '</div>';
}
echo $this->fetch('content');
echo $this->fetch('lowerSection');
echo $this->element('footer');
?>
