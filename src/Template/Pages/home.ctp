<?= $this->element('slider/home'); ?>
<div class="container">

    <h2>Ranking</h2>
    <div class="row ranking">
        <?php if ($categories): ?>
            <?php foreach ($categories as $oneCategory): ?>
                <div class="col-lg-3 up">

    <?= $this->Html->link(
        "<div class=\"card\"><i class=\"{$oneCategory->icon}\"></i><h4>{$oneCategory->name}</h4></div>",
        ['controller' => 'items', 'action' => 'rank', $oneCategory->name],
        [
            'class' => '',
            'escapeTitle' => false
        ]); ?>
                
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>


    <h2>Artykuły</h2>
    <div class="row">
        <div class="col-lg-4 mb-4">
            <div class="card">
                <a href="#"><img class="card-img-top"
                                 src="https://images.pexels.com/photos/261628/pexels-photo-261628.jpeg?h=350&auto=compress&cs=tinysrgb"
                                 alt=""></a>
                <div class="card-body">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p class="card-date">16 sierpień 2017</p>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse
                        necessitatibus neque.</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 mb-4">
            <div class="card">
                <a href="#"><img class="card-img-top"
                                 src="https://images.pexels.com/photos/196656/pexels-photo-196656.jpeg?h=350&auto=compress&cs=tinysrgb"
                                 alt=""></a>
                <div class="card-body">
                    <h4>Consectetur adipisicing elit</h4>
                    <p class="card-date">18 sierpień 2017</p>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ipsam eos,
                        nam perspiciatis natus commodi similique totam consectetur praesentium molestiae atque
                        exercitationem ut consequuntur, sed eveniet, magni nostrum sint fuga.</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 mb-4">
            <div class="card">
                <a href="#"><img class="card-img-top"
                                 src="https://images.pexels.com/photos/248528/pexels-photo-248528.jpeg?h=350&auto=compress&cs=tinysrgb"
                                 alt=""></a>
                <div class="card-body">
                    <h4>Sapiente esse necessitatibus neque</h4>
                    <p class="card-date">16 sierpień 2017</p>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse
                        necessitatibus neque.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <a class="btn btn-lg btn-secondary btn-block" href="#">WIĘCEJ ARTYKUŁÓW</a>
        </div>
        <div class="col-md-4">
        </div>
    </div>


    <h2>Nasze testy</h2>

    <div id="tests">

        <div class="row">
            <div class="col-lg-6 mb-6 up">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-6 mb-6 all">
                            <a href="#">
								<img class="card-img-top" src="https://images.pexels.com/photos/331684/pexels-photo-331684.jpeg?h=350&auto=compress&cs=tinysrgb" alt="">
								<img src="img/playButton.png" class="play_button">
							</a>
                        </div>
                        <div class="col-lg-6 mb-6 arr">
                            <div class="card-body">
                                <h4>Lorem ipsum dolor sit amet</h4>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Reiciendis ipsam eos, consectetur adipisicing elit.</p>
   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-6 up">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-6 mb-6 all">
                            <a href="#">
								<img class="card-img-top" src="https://images.pexels.com/photos/331684/pexels-photo-331684.jpeg?h=350&auto=compress&cs=tinysrgb" alt="">
								<img src="img/playButton.png" class="play_button">
							</a>
                        </div>
                        <div class="col-lg-6 mb-6 arr">
                            <div class="card-body">
                                <h4>Aliquam sit amet</h4>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Reiciendis ipsam eos.</p>
    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 mb-6 up">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-6 mb-6 all">
                            <a href="#">
								<img class="card-img-top" src="https://images.pexels.com/photos/331684/pexels-photo-331684.jpeg?h=350&auto=compress&cs=tinysrgb" alt="">
								<img src="img/playButton.png" class="play_button">
							</a>
                        </div>
                        <div class="col-lg-6 mb-6 arr">
                            <div class="card-body">
                                <h4>Maecenas pulvinar lorem</h4>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Reiciendis ipsam eos.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-6 up">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-6 mb-6 all">
                            <a href="#">
								<img class="card-img-top" src="https://images.pexels.com/photos/331684/pexels-photo-331684.jpeg?h=350&auto=compress&cs=tinysrgb" alt="">
								<img src="img/playButton.png" class="play_button">
							</a>
                        </div>
                        <div class="col-lg-6 mb-6 arr">
                            <div class="card-body">
                                <h4>Consectetur adipisicing elit</h4>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Reiciendis ipsam eos, consectetur adipisicing elit.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <a class="btn btn-lg btn-secondary btn-block" href="#">WIĘCEJ TESTÓW</a>
            </div>
            <div class="col-md-4">
            </div>
        </div>
    </div>

    <h2>Promocje</h2>

    <div class="row">
        <div class="col-lg-2 col-sm-6 portfolio-item">
            <div class="card">
                <a href="#"><img class="card-img-top"
                                 src="https://www.proximus.be/dam/cdn/sites/iportal/images/products/device/p/px-huawei-nova-gold-plus-sim/detail/px-huawei-nova-gold-plus-sim-XS-1.jpg"
                                 alt=""></a>
                <div class="card-body">
                    <h4 class="card-title"><a href="#">Huawei P9 Lite</a></h4>
                    <p class="card-text"><b>1852 zł</b> &nbsp;&nbsp;<s>2099 zł</s></p>
                </div>
                <a href="#" class="btn btn-primary">Kup teraz</a>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6 portfolio-item">
            <div class="card">
                <a href="#"><img class="card-img-top"
                                 src="http://www.bell.ca/Styles/wireless/all_languages/all_regions/catalog_images/iPhone_7/iPhone7_MatBlk_lrg1_en.png"
                                 alt=""></a>
                <div class="card-body">
                    <h4 class="card-title"><a href="#">iPhone 5S 16gb</a></h4>
                    <p class="card-text"><b>1520 zł</b> &nbsp;&nbsp;<s>1900 zł</s></p>
                </div>
                <a href="#" class="btn btn-primary">Kup teraz</a>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6 portfolio-item">
            <div class="card">
                <a href="#"><img class="card-img-top"
                                 src="https://boulanger.scene7.com/is/image/Boulanger/bfr_overlay?layer=comp&$t1=&$product_id=Boulanger/8806088680361_h_f_l_0&wid=400&hei=400"
                                 alt=""></a>
                <div class="card-body">
                    <h4 class="card-title"><a href="#">Samsung Galaxy S3</a></h4>
                    <p class="card-text"><b>970 zł</b> &nbsp;&nbsp;<s>1299 zł</s></p>
                </div>
                <a href="#" class="btn btn-primary">Kup teraz</a>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6 portfolio-item">
            <div class="card">
                <a href="#"><img class="card-img-top"
                                 src="http://s4.thingpic.com/images/N1/o4WCw7NqXhrUsLa6nYcUpcoT.jpeg" alt=""></a>
                <div class="card-body">
                    <h4 class="card-title"><a href="#">Huawei P10</a></h4>
                    <p class="card-text"><b>230 zł</b> &nbsp;&nbsp;<s>1000 zł</s></p>
                </div>
                <a href="#" class="btn btn-primary">Kup teraz</a>
            </div>
        </div>
		<div class="col-lg-2 col-sm-6 portfolio-item">
            <div class="card">
                <a href="#"><img class="card-img-top"
                                 src="https://www.proximus.be/dam/cdn/sites/iportal/images/products/device/p/px-huawei-nova-gold-plus-sim/detail/px-huawei-nova-gold-plus-sim-XS-1.jpg"
                                 alt=""></a>
                <div class="card-body">
                    <h4 class="card-title"><a href="#">Huawei P9 Lite</a></h4>
                    <p class="card-text"><b>1852 zł</b> &nbsp;&nbsp;<s>2099 zł</s></p>
                </div>
                <a href="#" class="btn btn-primary">Kup teraz</a>
            </div>
        </div>
        <div class="col-lg-2 col-sm-6 portfolio-item">
            <div class="card">
                <a href="#"><img class="card-img-top"
                                 src="http://www.bell.ca/Styles/wireless/all_languages/all_regions/catalog_images/iPhone_7/iPhone7_MatBlk_lrg1_en.png"
                                 alt=""></a>
                <div class="card-body">
                    <h4 class="card-title"><a href="#">iPhone 5S 16gb</a></h4>
                    <p class="card-text"><b>1520 zł</b> &nbsp;&nbsp;<s>1900 zł</s></p>
                </div>
                <a href="#" class="btn btn-primary">Kup teraz</a>
            </div>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <a class="btn btn-lg btn-secondary btn-block" href="#">WIĘCEJ PROMOCJI</a>
        </div>
        <div class="col-md-4">
        </div>
    </div>

</div>