<div class="container">
	<div class="page_title">
		<h2>Logowanie</h2>
	</div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?= $this->Form->create('Users', ['class' => 'form']) ?>
            <div class="form-group">
                <?= $this->Form->control('username', [
                    'class' => 'form-control',
                    'label' => [
                        'text' => __('Adres email'),
                        'class' => 'label'
                    ],
                    'required' => true,
                ]); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('password', [
                    'class' => 'form-control',
                    'label' => [
                        'text' => __('Hasło'),
                        'class' => 'label'
                    ],
                    'required' => true,
                ]); ?>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <?= $this->Form->button(__('Zaloguj'), ['class' => 'btn btn-primary']); ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>