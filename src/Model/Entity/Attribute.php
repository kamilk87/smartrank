<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use \Cake\ORM\TableRegistry;

/**
 * Attribute Entity
 *
 * @property int $id
 * @property int $points
 * @property string $description
 * @property int $feature_id
 * @property int $item_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Feature $feature
 * @property \App\Model\Entity\Item $item
 */
class Attribute extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    protected $_virtual = ['rank'];

    public function _getRank() {
        /**
         * @var  \App\Model\Table\AttributesTable $attributesTableObj
         */
        $attributesTableObj = TableRegistry::get('Attributes');
        $query = $attributesTableObj->find('all')
                ->where(['feature_id' => $this->feature_id]);
        $maxPoints = $query->select(['maxPoints' => $query->func()->max('points')])->first();
        return number_format(($this->points / $maxPoints->maxPoints) * 100,2);
    }

}
