<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Item Entity
 *
 * @property int $id
 * @property string $image
 * @property string $name
 * @property float $price
 * @property string $description
 * @property int $category_id
 * @property string $link
 * @property float $points
 * @property string $dir
 * @property string $type
 * @property int $size
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Attribute[] $attributes
 */
class Item extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    protected $_virtual = ['rank', 'position'];

    function _getPosition() {
        $tableObj = TableRegistry::get('Items');
        $results = [];
        $counter = 1;
        $items = $tableObj->find('all')
                ->cache('Items')
                ->contain(['Attributes'])
                ->all();
        foreach ($items as $oneItem) {
            $results[$oneItem->id] = $oneItem->rank;
        }
        arsort($results);
        foreach ($results as $key => $value) {
            if ($key == $this->id) {
                break;
            }
            $counter++;
        }
        return $counter;
    }

    function _getRank() {
        $counter = 1;
        $pointsSum = 0;
        if (isset($this->attributes)) {
            foreach ($this->attributes as $oneAttribute) {
                $pointsSum += $oneAttribute->rank;
                $counter++;
            }
            return number_format($pointsSum / $counter, 2);
        }
    }

}
